package com.suriya.androidcomponents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckedTextView
import android.widget.Toast

class CheckedTextViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checked_text_view)
        val checkTextView = findViewById<CheckedTextView>(R.id.che_txt_view)
        checkTextView.isChecked = false
        checkTextView.setCheckMarkDrawable(android.R.drawable.checkbox_off_background)
        checkTextView.setOnClickListener {
            checkTextView.isChecked = !checkTextView.isChecked
            checkTextView.setCheckMarkDrawable(
                if(checkTextView.isChecked)
                    android.R.drawable.checkbox_on_background
                else
                    android.R.drawable.checkbox_off_background
            )
            val msg = getString(R.string.msg_shown) + " "+ getString(
                if(checkTextView.isChecked) R.string.checked else R.string.unchecked
            )
            Toast.makeText(CheckedTextViewActivity@this,msg,Toast.LENGTH_LONG).show()
        }

    }
}