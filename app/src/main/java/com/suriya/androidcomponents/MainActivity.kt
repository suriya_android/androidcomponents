package com.suriya.androidcomponents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnTextView1 = findViewById<Button>(R.id.btn_txtView1)
        btnTextView1.setOnClickListener{
            val intent = Intent(MainActivity@this,TextViewActivity :: class.java)
            startActivity(intent)
        }

        val btnTextView2 = findViewById<Button>(R.id.btn_txtView2)
        btnTextView2.setOnClickListener{
            val intent = Intent(MainActivity@this,AutoCompleteTextViewActivity :: class.java)
            startActivity(intent)
        }

        val btnTextView3 = findViewById<Button>(R.id.btn_txtView3)
        btnTextView3.setOnClickListener{
            val intent = Intent(MainActivity@this,CheckedTextViewActivity :: class.java)
            startActivity(intent)
        }

        val btnTextView4 = findViewById<Button>(R.id.btn_txtView4)
        btnTextView4.setOnClickListener{
            val intent = Intent(MainActivity@this,HorizontalScrollviewActivity :: class.java)
            startActivity(intent)
        }

        val btnTextView5 = findViewById<Button>(R.id.btn_txtView5)
        btnTextView5.setOnClickListener{
            val intent = Intent(MainActivity@this,ListViewActivity :: class.java)
            startActivity(intent)
        }

        val btnTextView6 = findViewById<Button>(R.id.btn_txtView6)
        btnTextView6.setOnClickListener{
            val intent = Intent(MainActivity@this,RadioButtonActivity :: class.java)
            startActivity(intent)
        }



    }
}