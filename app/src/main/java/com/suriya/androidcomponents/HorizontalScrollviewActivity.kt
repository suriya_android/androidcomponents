package com.suriya.androidcomponents

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class HorizontalScrollviewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_horizontal_scrollview)
        val image1 = findViewById<ImageView>(R.id.image1)
        val image2 = findViewById<ImageView>(R.id.image2)
        val image3 = findViewById<ImageView>(R.id.image3)
        val image4 = findViewById<ImageView>(R.id.image4)
        val image5 = findViewById<ImageView>(R.id.image5)
        val image6 = findViewById<ImageView>(R.id.image6)
        val image7 = findViewById<ImageView>(R.id.image7)
        val image8 = findViewById<ImageView>(R.id.image8)
        val image9 = findViewById<ImageView>(R.id.image9)
        val image10 = findViewById<ImageView>(R.id.image10)
        image1.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image2.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image3.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image4.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image5.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image6.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image7.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image8.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image9.setOnClickListener{
            setImageShow(it as ImageView)
        }
        image10.setOnClickListener{
            setImageShow(it as ImageView)
        }
    }

    fun setImageShow(view:ImageView){
        val imageShow = findViewById<ImageView>(R.id.img_show)
        imageShow.setImageDrawable(view.drawable)
    }
}