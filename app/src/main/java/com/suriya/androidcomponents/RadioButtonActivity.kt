package com.suriya.androidcomponents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_radio_button.*

class RadioButtonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_button)
        button.setOnClickListener {
            val id = rad_group.checkedRadioButtonId
            if(id!=-1){
                val radio = findViewById<RadioButton>(id)
                Toast.makeText(applicationContext,"On button click : ${radio.text}",Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(applicationContext,"On button click : nothing selected",Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun radioButtonClick(view: View){
        val radio = findViewById<RadioButton>(rad_group.checkedRadioButtonId)
        Toast.makeText(applicationContext,"On click : ${radio.text}",Toast.LENGTH_SHORT).show()
    }
}