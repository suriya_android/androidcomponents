package com.suriya.androidcomponents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import org.w3c.dom.Text

class ListViewDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view_detail)
        val name:String = intent.getStringExtra("name")?:"Unknown"
        val textViewDetail = findViewById<TextView>(R.id.txt_view_detail)
        Log.d(name,"get Value from another Activity")
        textViewDetail.text = name
    }
//    YAY
}