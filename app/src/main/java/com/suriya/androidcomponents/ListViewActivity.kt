package com.suriya.androidcomponents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class ListViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        val arrayAdapter : ArrayAdapter<*>
        val users = arrayOf(
            "Virat Kohli","Rohit Sharama", "Steve Smith",
            "Kane Williamson", "Ross Taylor","Ayanami Ray",
            "Todd Howard", "Aloha 8 bit","Ezkiddo Mo",
            "Virat Kohli","Rohit Sharama", "Steve Smith",
            "Kane Williamson", "Ross Taylor","Ayanami Ray",
            "Todd Howard", "Aloha 8 bit","Ezkiddo Mo"
        )

        val userList = findViewById<ListView>(R.id.userList)
        arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,users)
        userList.adapter = arrayAdapter
        userList.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(ListViewActivity@this, users[position],Toast.LENGTH_SHORT).show()
        }
    }
}